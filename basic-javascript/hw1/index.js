// 1. За допомогою var, let і const.

// 2. Функція prompt - це коли тебе запитують, ти вводиш значення і воно виводиться в консолі, та функція confirm - це коли тобі задають питання і ти відповідаєш так і виводиться true, або cancel і виводиться false.

// 3. Неявне перетворення це автоматичне перетворення типів.

const name = "Vanya"
const admin = name;
console.log(admin);

const days = +prompt("Введіть число дня тиждня");
const hour = days * 24;
const minutes = hour * 60;
const second = minutes * 60;
console.log(second);

let question = prompt("Чим ти сьогодні займався?");
console.log(question);