// 1. Єкранування - це коли добавляєш \ перед символами для пошуку спеціальних символів [ ] \ ^ $ . | ? * + ( ), нам потрібно перед ними добавити \ .

function createNewUser() {
    let firstName = prompt("Введіть своє ім'я")
    let lastName = prompt("Введіть свою фамілію")
    let birthDate = prompt("Введіть свою дату народження dd.mm.yyyy")

    const nowDate = new Date();
    let arr = birthDate.split(".");

    const newUser = {
        name: firstName,
        surname: lastName,
        getLogin: function() {
            login = this.name[0] + this.surname
            return login.toLowerCase()
        },

        birthday: new Date(arr[2], arr[1], arr[0]),
        getAge: function() {
          let age = ((nowDate - this.birthday)/(24 * 3600 * 365.25 * 1000));
          return Math.floor(age);
        },

        getPassword: function() {
            let paswoword = this.name[0].toUpperCase() + this.surname.toLowerCase() + arr[2];
            return paswoword;
          },
    }
    return newUser
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
