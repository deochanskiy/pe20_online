const btn = document.querySelector('.change-btn');
const body = document.getElementsByTagName('body');
const change = localStorage.getItem('change'); 
window.onload = function () {
    if (localStorage.getItem('change') !== null) {
        body[0].classList = change;
}}
btn.addEventListener('click', function(event) {
    if (localStorage.getItem('change')){
        body[0].classList.remove('change');
        localStorage.removeItem('change');
    } else {
        body[0].classList.add('change');
        localStorage.setItem('change', 'crimson')
    };
})