// 1. ОЦикл forEach виконую функцію для кожного елемента в масиві.

function filterBy(array, ...type) {
    const filterResult = []

    for(let i = 0; i < type.length; i++) {
        if (typeof type[i] === array) {
            filterResult.push(type[i])
        }
    }

    return filterResult
}


let res = filterBy('string', 534, 54325, 5435235, '5523','world', undefined, 'rock', null, {}, 777, '777', 'end')

console.log(res)