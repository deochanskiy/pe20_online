const tabs = document.querySelector(".tabs");
const tabsName = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-text");

tabs.addEventListener("click", (event) => {
    let getTarget = event.target;
    
    for (let i = 0; i < tabsName.length; i++) {
        if (getTarget === tabsName[i]) {
            tabsName[i].classList.add("active");
            tabsContent[i].classList.add("tabs-list-watched");
        } else {
            tabsName[i].classList.remove("active");
            tabsContent[i].classList.remove("tabs-list-watched");
        }
    }
})
