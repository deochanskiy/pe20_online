// 1. Функції потрібні наприклад для створення алгоритму, який буде повторюватись в коді.

// 2. Аргумент в функцію зручно передавати, для того щоб, цей же аргумент брати для використовування в іншій функції.

// 3. Повернення результату виконання функції.

let firstNum = +prompt("Enter the your first number")
let secondNum = +prompt("Enter the your second number")
let operation = prompt("Enter the your operation +, -, *, /.")


function execution(firstNum, secondNum, operation) {
    let result

    if (operation === "+") {
        result = firstNum + secondNum
    } else if (operation === "-") {
        result = firstNum - secondNum
    } else if (operation === "/" && secondNum !== 0) {
        result = firstNum / secondNum
    } else {
        result = firstNum * secondNum
    }

    return result
} 

console.log(execution(firstNum, secondNum, operation))