// Our Services

const nameServices = document.querySelectorAll(".service-list-item");
const textOfDescription = document.querySelectorAll(".description");


for (let i = 0; i < nameServices.length; i++){
    nameServices[i].addEventListener("click", () => {
        nameServices.forEach((item) => {
            item.classList.remove("active")
        })
        nameServices[i].classList.toggle("active")
        textOfDescription.forEach((item) => {
            item.classList.remove("active")
        })
        textOfDescription[i].classList.toggle("active")
    })
}

document.addEventListener("DOMContentLoaded", function(event) {
    const changeOfBackground = document.querySelector("service-list-item")
    changeOfBackground.
    this.addEventListener("click", () => {
        document.changeOfBackground.style.backgroundColor = "#18CFAB";
    })
})

// Our Amazing Work

const workList = document.querySelector(".amazing-work-list"),
      workItems = document.querySelectorAll(".work-squares-item");

function filter(){
    workList.addEventListener("click", event => {
        const targetId = event.target.dataset.id

        switch(targetId) {
            case "all":
                addImage("work-squares-item")
                break
            case "graphic-design":
                addImage(targetId)
                break
            case "web-design":
                addImage(targetId)
                break
            case "landing-pages":
                addImage(targetId)
                break
            case "wordpress":
                addImage(targetId)
                break
        }
    })
}

filter();

function addImage(className) {
    workItems.forEach(item => {
        if(item.classList.contains(className)) {
            item.style.display = "block"
        } else {
            item.style.display = "none"
        }
    })
}


// Our Amazing Work => Loade More Images

// function addImages(){
//     const workSquares = document.querySelector(".work-squares");
//     const squaresImg = document.querySelector(".work-squares-img").cloneNode(true);
//     const graphicDesign = squaresImg.querySelector(".graphic-design");
//     const webDesign = squaresImg.querySelector(".web-design");
//     const landingPages = squaresImg.querySelector(".landing-pages");
//     const wordpress = squaresImg.querySelector(".wordpress");

//     graphicDesign.querySelector('img').src = `./image-graphic-design/graphic-design${workSquares.children.length + 1}.jpg`;
//     webDesign.querySelector('img').src = `./image-web-design/web-design${workSquares.children.length + 1}.jpg`;
//     landingPages.querySelector('img').src = `./image-landing-pages/landing-pages${workSquares.children.length + 1}.jpg`;
//     wordpress.querySelector('img').src = `./image-wordpress/wordpress${workSquares.children.length + 1}.jpg`;

//     workSquares.appendChild(squaresImg);
// }

// for (i = 0; i < squaresImg.length; i++) addImages()

// const loadMore = document.querySelector(".load-more-button-active");

// loadMore.addEventListener('click', evt => {
//     evt.preventDefault();
//     loadMore.style.display = 'none';
//     for (i = 0; i < squaresImg.length; i++) addImages();
// })

// AddImages()


// Slider Profile Actor

let currentSlide = 0;
const sliderImg = document.querySelectorAll(".slider-image");
const profileContent = document.querySelectorAll(".profile-contant");
const right = document.getElementById("sliderRight");
const left = document.getElementById("sliderLeft");

for(let i = 0; i < sliderImg.length; i++) {
    sliderImg[i].onclick = function () {
        currentSlide = i;
        document.querySelector(".profile-contant.container.active").classList.remove('active');
        document.querySelector(".slider-image.active").classList.remove('active');
        sliderImg[currentSlide].classList.add('active');
        profileContent[currentSlide].classList.add('active');
    }
}

right.onclick = function() {
    nextSlide(currentSlide);
};

left.onclick = function() {
    previousSlide(currentSlide);
};

function nextSlide() {
    goToSlide(currentSlide+1);
}

function previousSlide() {
    goToSlide(currentSlide-1);
}

function goToSlide(n){
    hideSlides();
    currentSlide = (n+profileContent.length)%profileContent.length;
    showSlides();
}

function hideSlides(){
    profileContent[currentSlide].className = "profile-contant container";
    sliderImg[currentSlide].className = "slider-image";
}

function showSlides(){
    profileContent[currentSlide].className = "profile-contant container active";
    sliderImg[currentSlide].className = "slider-image active";
}

